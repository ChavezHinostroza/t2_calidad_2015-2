﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Interfaces
{
   public interface InterfacePoso
    {
        List<Pozo> All();
        List<Pozo> ByQueryAll(string query);
        void Store(Pozo poso);
        Pozo AsignarPersonaPozo(Pozo pozo, Persona persona);
        
    }
}
