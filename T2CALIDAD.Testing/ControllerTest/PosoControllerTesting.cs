﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using Modelos;
using Moq;
using NUnit.Framework;
using T2CALIDAD;
using System.Web.Mvc;
using T2CALIDAD.Controllers;
using Validators.PosoValidators;

namespace T2CALIDAD.Testing.ControllerTest
{
   [TestFixture]
   public class PosoControllerTesting
    {
        
        [Test]
        public void TestCreateReturnView()
        {
            var controller = new PozoController(null, null);

            var view = controller.Create();

            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("Create", view.ViewName);
        }

        [Test]
        public void TestPostCreateOKReturnRedirect()
        {
            var pozo = new Pozo();

            var repositoryMock = new Mock<InterfacePoso>();

            repositoryMock.Setup(o => o.Store(new Pozo()));

            var validatorMock = new Mock<PosoValidator>();

            validatorMock.Setup(o => o.Pass(pozo)).Returns(true);

            var controller = new PozoController(repositoryMock.Object, validatorMock.Object);

            var redirect = (RedirectToRouteResult)controller.Create(pozo);

            Assert.IsInstanceOf(typeof(RedirectToRouteResult), redirect);
        }

        [Test]
        public void TestPostCreateCallStoreMethodFromRepository()
        {
            var pozo = new Pozo();

            var repositoryMock = new Mock<InterfacePoso>();

            var validatorMock = new Mock<PosoValidator>();

            validatorMock.Setup(o => o.Pass(pozo)).Returns(true);

            repositoryMock.Setup(o => o.Store(pozo));

            var controller = new PozoController(repositoryMock.Object, validatorMock.Object);

            var redirect = controller.Create(pozo);

            repositoryMock.Verify(o => o.Store(pozo), Times.Once());

        }

        [Test]

        public void TestPostCreateReturnViewWithErrorsWhenValidationFail()
        {
            var pozo = new Pozo { };

            var mock = new Mock<PosoValidator>();

            mock.Setup(o => o.Pass(pozo)).Returns(false);

            var controller = new PozoController(null, mock.Object);

            var view = controller.Create(pozo);

            Assert.IsInstanceOf(typeof(ViewResult), view);
        }

        [Test]
        public void TestAsignarPersonaAPozo()
        {
            var pozo = new Pozo { };

            var mock = new Mock<InterfacePoso>();
            var persona = new Persona { Id=1, name ="JhonKeilvin"};
            mock.Setup(o => o.All()).Returns(new List<Pozo>());
            mock.Setup(o => o.ByQueryAll("")).Returns(new List<Pozo>());
            mock.Setup(o => o.AsignarPersonaPozo(pozo,persona)).Returns(new Pozo());
            var mockv = new Mock<PosoValidator>();

            mockv.Setup(o => o.Pass(pozo)).Returns(false);
            var controller = new PozoController(mock.Object, mockv.Object);
            var view = controller.AsignarPozoPersona(pozo,persona);
            Assert.IsInstanceOf(typeof(ViewResult), view);
            Assert.AreEqual("AsignarPozoPersona", view.ViewName);
        }

    }
}
