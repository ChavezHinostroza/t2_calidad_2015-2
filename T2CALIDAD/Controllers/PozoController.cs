﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Modelos;
using Interfaces;
using Validators.PosoValidators;


namespace T2CALIDAD.Controllers
{
    public class PozoController : Controller
    {
        //
        // GET: /Pozo/
        private InterfacePoso repository;
        private PosoValidator validator;

        public PozoController(InterfacePoso repository, PosoValidator validator)
        {
            this.repository = repository;
            this.validator = validator;
        }

        public ViewResult Index(String query = "")
        {
            var datos = repository.ByQueryAll(query);
            return View("ListP");
        }
        [HttpGet]
        public ViewResult Create()
        {
            return View("Create");
        }

        [HttpPost]
        public ActionResult Create(Pozo pozo)
        {

            if (validator.Pass(pozo))
            {
                repository.Store(pozo);

                TempData["UpdateSuccess"] = "Se envio el Correo Satisfactoriamente";

                return RedirectToAction("Index");
            }

            return View("ListP", pozo);
        }
        [HttpPost]
        public ViewResult AsignarPozoPersona(Pozo pozo, Persona persona)
        {
            var datos = repository.AsignarPersonaPozo(pozo,persona);
            return View("AsignarPozoPersona");

        }

    }
}
